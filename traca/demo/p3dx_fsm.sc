// Single component information !!!

// component FSM
println(Cyan("FSM States Transitions"))
// for (cpt <- trace.components) {
// println(Blue(cpt.name))
// cpt.fsm.printTransitions
// }

// trace.components.safety_pilot.fsm.printTransitions
println(Blue(trace.components.detect_component.name))
trace.components.detect_component.fsm.printTransitions
println(Blue(trace.components.analyze_component.name))
trace.components.analyze_component.fsm.printTransitions
println

// component States Duration
println(Cyan("FSM States Durations"))
// for (cpt <- trace.components) {
// println(Blue(cpt.name))
// cpt.fsm.printStatesDuration
// }
println(Blue(trace.components.detect_component.name))
trace.components.detect_component.fsm.printStatesDuration
println(Blue(trace.components.analyze_component.name))
trace.components.analyze_component.fsm.printStatesDuration
println

// component FSM
// println(Cyan("FSM States Durations"))
// for (cpt <- trace.components) {
// println(Blue(cpt.name))
// cpt.fsm.printStatesTiming
// }
println(Blue(trace.components.detect_component.name))
trace.components.detect_component.fsm.printStatesTiming
// println(Blue(trace.components.analyze_component.name))
// trace.components.analyze_component.fsm.printStatesTiming
println
