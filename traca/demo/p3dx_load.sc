// Load P3DX Trace
:open trace/ust/uid/1000/64-bit/
println("Trace loaded")
println
// Trace Statistics
println(Cyan("Trace Statistics"))
println("Trace length     : " + trace.size)
// println("Trace start      : " + trace.start)
// println("Trace finish     : " + trace.finish)
// println("Trace duration   : " + trace.duration)
println("Trace event freq.: " + (trace.duration / trace.size))
println

// > trace.pointwise.take(5)
