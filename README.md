# MAUVE Tools

Provides some tools for the MAUVE Toolchain.

Current available tools include:

* the runtime **Deployer**, used to deploy interactively a MAUVE architecture
* **TracA**, a tool to analyse MAUVE traces
