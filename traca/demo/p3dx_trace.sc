// ex: trace.components
println

// no migration !
println(Cyan("Components Cpu"))
trace.printComponentsCpu
println

println(Cyan("Cpu Components"))
trace.printCpuComponents
println

println(Cyan("Cpu Load"))
trace.printCpuLoad
println

// CPU + SCHED_FIFO + PIP
println(Cyan("Components Preemptions"))
trace.printComponentsPreemptions
println

// duration % not cpu usage
println(Cyan("Components Duration"))
trace.printComponentsDuration
println
