// Mutiple component information !!!

def result(b: Boolean): String = if (b) Green("true") else Red("false")

println(Cyan("Predicates"))
val aware = Predicate("aware", ExecutionBegin("safety_pilot", "aware"))
val obstacle = Predicate("obstacle", ExecutionBegin("safety_pilot", "stopping"))
val stopped = Predicate("stopped", ExecutionBegin("safety_pilot", "waiting"))

val detected = Predicate("detected", ExecutionBegin("detect_component", "send"))
val analyzed = Predicate("analyzed", ExecutionEnd("analyze_component", "analyzeData"))

println
val phi: BoolExpr = G(obstacle implies F(stopped))
println(Cyan("Safety Stop"))
println(phi.toString + " = " + result(trace.pointwise.solve(phi)))
println

val phi = G(obstacle implies save("x", F(stopped and clock("x") <= 200.ms)))
println(Cyan("Safety Stop in less than 100 ms"))
println(phi.toString + " = " + result(trace.pointwise.solve(phi)))
println

val detect = ContextMemory(detected, "detect")
val analyze = ContextMemory(analyzed, "analyze")
val ctx = new Context
def display_context(x: Context) = {
  println((x.detect.time - trace.start).toString + "\t -> " + (x.analyze.time - trace.start) + "\t [" + (x.analyze.time - x.detect.time) + "]")
}
def display(set: Set[Context]) = {
  for (x <- set) display_context(x)
}
println

val phi = F(detect and F(analyze))
println(Cyan("Detection and analyse (first one)"))
println(phi.toString + " = " + result(trace.pointwise.solve(phi, ctx)))
println
println("display one context:")
display_context(ctx)
println

val phi = detect and F(analyze)
println(Cyan("Detection and analyse all"))
val set = trace.pointwise.all(phi)
println
println("display all:")
display(set)
println
